<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
     body{
        height: 100%;
        margin: 0;
        overflow:hidden;

    }
    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        max-height: 50px;
        overflow: hidden;
        color: white;
        font-family: calibri;
        background-color: #071426;
    }
    li {
        float: left;
        position: relative;
        left: 32%;
    }
    li input {
        display: inline;
        color: #888e94;
        border: none;
        text-align: center;
        width: 100px;
        height:50px;
        background-color: transparent;
        text-decoration: none;
    }
    li input:hover:not(.active) {
        color: white;
    }
    .active {
       color: #4CAF50;
    }
    .acc{

    }
    #isi{
        overflow: auto;
        margin: 0px;
        position:relative;
        width: 98.5%;
        height:85vh;
        max-height: auto;
        background-image: url("{{asset('img/nav/store.gif')}}");
        background-repeat: no-repeat,repeat;
        background-position: center;
        background-size: cover;
        /* background-color:#226394; */
        background-color:#253d4f;
        font-family: calibri;
        padding:5px 10px 5px 10px;
        color: white;
        float: left;
    }
    .fg{
        border-radius: 10px;
        position: relative;
        width: 20%;
        height: auto;
        display: inline-block;
        margin-left:40%;
        margin-top: 3%;
        margin-bottom: 1%;
        background-color:#102236;
        color: white;
        padding: 10px;
    }    
    .fg form{
        position: inherit;
        float: left;  
        left:18%;      
    }
    .input {
        border: none;
        background-color: transparent;
        border-bottom: solid white 1px;
        color:  #00fcbd;
    }
    .input:focus{
        border-bottom: solid green 1px;
    }
    .submitlogin{
        border: none;
        position: relative;
        height: 25px;
        width: 168px;
        color:white;
        background-color:#4CAF50;
        border-radius: 20px
    }
    .submitreg{
        border: none;        
        background-color: transparent;
        color: #346beb;
        position: relative;
        float:left;
        right: 16%;
    }
    .have{
        background-color:#4CAF50;
        color: white;
        text-decoration: none;
        float: right;
        padding: 2px;
        border-radius: 3px;
        top: 0;
        position: sticky;
    }
    .have input{
        text-decoration: none;
        border: none;
        background-color:transparent;
        color: #001d6b;
    }
    #footer{
        color: #888e94;
        font-family: calibri;
        background-color: #071426;
        overflow: hidden;
        padding-left: 45%;
        padding-bottom:2%;
    }
    #err{
        position: relative;
        color:#e04e2d;                                      
    }
    </style>
</head>
<body>
<ul>
<form action="{{url('store/login')}}" method="get" >
<li><a href="{{url('store/home')}}"><img src="{{asset('img/nav/logoetoys.png')}}" alt="" width="100" height="50"></a></li>
    <li><input type="submit" name="home" value="Store"></li>
    <li><input type="submit" name="lib" value="Library"></li>
    <li><input type="submit" name="prof" value="Profile"></li>    
    <li><a style="position:absolute; float: left;" href="{{url('store/cart')}}"><img src="{{url('img/cart/cart.png')}}" alt="" width="50" height="50"></a>0</li>
 </form>
</ul>    
    <div id="isi">
    <div class="fg">
    <center><h1>Login</h1></center>    
        @if($errors->any())        
        @foreach($errors->all() as $err)            
            <center><p id="err">{{$err}}</p></center>
        @endforeach        
        @endif                
    <form action="{{url('store/processLogin')}}" method="post">
        @csrf        
        Username <br> <input class="input" type="text" name="username" id=""></center><br><br>
        Password <br> <input class="input" type="text" name="pass" id=""><br><br>              
        <input class="submitlogin" type="submit" value="Login" name="loginButton">
        <br><br>                
        <input class="submitreg" type="submit" value="Don't have any account? Create one free" name="registerButton">                
    </form>    
    </div>
    </div>
  <div id="footer">
   Copyright FAIPROJECT 2019.
  </div>
</body>
</html>
