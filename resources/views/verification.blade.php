<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Verification</title>
    <style>
    body{
        height: 100%;
        margin: 0;
        overflow:hidden;
    }
    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        max-height: 50px;
        overflow: hidden;
        color: white;
        font-family: calibri;
        background-color: #071426;
    }
    li {
        float: left;
        position: relative;
        left: 32%;
    }
    li input {
        display: inline;
        color: #888e94;
        border: none;
        text-align: center;
        width: 100px;
        height:50px;
        background-color: transparent;
        text-decoration: none;
    }
    li input:hover:not(.active) {
        color: white;
    }
    .active {
       color: #4CAF50;
    }
    .acc{

    }
    #isi{
        overflow: auto;
        margin: 0px;
        position:relative;
        width: 98.5%;
        height:85vh;
        max-height: auto;
        background-image: url("{{url('img/nav/store.gif')}}");
        background-repeat: no-repeat,repeat;
        background-position: center;
        background-size: cover;
        background-color:#253d4f;
        font-family: calibri;
        padding:5px 10px 5px 10px;
        color: white;
        float: left;
    }
    .fg{
        border-radius: 10px;
        position: relative;
        width: 40%;
        max-width:auto;
        height: auto;
        opacity: 95%;
        display: inline-block;
        margin-left:30%;
        margin-top: 3%;
        margin-bottom: 1%;
        background-color:#102236;
        color: white;
        padding: 5px;
    }    
    .fg form{
        position: inherit;
        float: left;
        width: 100%;
    }   
    .input {
        border: none;
        background-color: transparent;
        border-bottom: solid white 1px;
        color:  #00fcbd;
        width: 100%;
    }
    .input:focus{
        border-bottom: solid green 1px;
    }
    .submit{
        border: none;
        position: relative;
        height: 25px;
        width: 168px;
        color:white;
        background-color:#4CAF50;        
        border-radius: 20px
    }
    .pay{
        float:left;
        position:relative;
        border:solid #00fcbd 2px;
        background:#253d4f;
        width: 150px;
        height: 70px;
        border-radius: 10px;
        margin-left: 3.5%;
    }
    .fgfoto{
        background-color:white;
        width: 325px;
        height: 225px;
        float:left;
        margin-right: 5px;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
    }
    .have{
        background-color:#4CAF50;
        color: white;
        text-decoration: none;
        float: right;
        padding: 2px;
        border-radius: 3px;
        top: 0;
        position: sticky;
        text-align: center;
    }
    .have input{
        text-decoration: none;
        border: none;
        background-color:transparent;
        color: #001d6b;
    }
    #footer{
        color: #888e94;
        font-family: calibri;
        background-color: #071426;
        overflow: hidden;
        padding-left: 45%;
        padding-bottom:2%;
    }
    #err{
        position: relative;
        color:#e04e2d;          
    }
    </style>
</head>
<body>
    <ul>
        <form action="{{url('store/processHeader')}}" method="post" >
            @csrf
            <li><a href="{{url('store/home')}}"><img src="{{asset('img/nav/logoetoys.png')}}" alt="" width="100" height="50"></a></li>
            <li><input type="submit" name="home" value="Store"></li>
            <li><input type="submit" name="lib" value="Library"></li>
            <li><input type="submit" name="prof" value="Profile"></li>            
            <li><a style="position:absolute; float: left;" href="{{url('store/cart')}}"><img src="{{url('img/cart/cart.png')}}" alt="" width="50" height="50"></a>
            <?php $cart=Session::get("cart")?>
                @if(Session::has("cart"))
                {{count($cart)}}
                @else
                0
                @endif
            </li>
        </form>
    </ul>
<div id="isi">
<div class="fg">
    <center><h2>Verification code has been send to {{$user->email}}<br></h2></center>
    <form action="{{url('store/processVerification')}}" method="POST">
        @csrf
        Enter Verification Code
        <input class="input" type="number" name="kode" id=""> <br><br>
        <center>
        <input class="submit" type="submit" value="Resend" name="resend" style="background-color:#346beb">
        <input class="submit" type="submit" name="verifKode" value="Verify">
        </center>
    </form>
</div>
</div>
    <div id="footer">
        Copyright FAIPROJECT 2019.
    </div>
</body>
</html>
