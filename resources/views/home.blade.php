<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
* {box-sizing: border-box}
.mySlides {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 900px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.act, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}

    body{
        height: 100%;
        margin: 0;
        overflow:hidden;
    }
    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        max-height: 50px;
        overflow: hidden;
        color: white;
        font-family: calibri;
        background-color: #071426;
    }
    li {
        float: left;
        position: relative;
        left: 32%;
    }
    li input {
        display: inline;
        color: #888e94;
        border: none;
        text-align: center;
        width: 100px;
        height:50px;
        background-color: transparent;
        text-decoration: none;
    }
    li input:hover:not(.active) {
        color: white;
    }
    .active {
       color: #4CAF50;
    }
    .acc{

    }
    #isi{
        overflow: auto;
        margin: 0px;
        position: relative;
        width: 100%;
        height:85vh;
        max-height: auto;
        background-image: url("{{asset('img/nav/home.gif')}}");
        max-height: auto;
        background-repeat: no-repeat,repeat;
        background-position: center;
        background-size: cover;
        background-color:#253d4f;
        font-family: calibri;
        padding:5px 10px 5px 10px;
        color: white;
        float: left;
    }
    .fg{
        border-radius: 10px;
        position: relative;
        width: 19.9%;
        height: 35%;
        opacity: 95%;
        display: inline-block;
        margin-left:10%;
        margin-top: 2%;
        margin-bottom: 5%;
        background-color:#102236;
        color: white;
        float: left;
        padding: 3px;
        opacity: 95%;
    }
    .fg:hover{
        background-color:#001d6b;
        opacity:100%;
        color: white;
    }
    .fgfoto{
        background-color:white;
        width: 100%;
        height: 150px;
        float:left;
        margin: 0px;
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
    }
    .have{
        background-color:#4CAF50;
        color: white;
        width: auto;
        height: auto;
        text-decoration: none;
        float: right;
        padding: 2px;
        border-radius: 3px;
        top:0;
        position: sticky;
    }
    .have form{
        float:right;
        padding-left: 1px;
    }
    .have input{
        text-decoration: none;
        border: none;
        bottom: 1px;
        margin-right: 4px;
        border-radius: 10px;
        background-color:#346beb;
        color: white;
    }
    #footer{
        height:15vh;
        color: #888e94;
        font-family: calibri;
        overflow:hidden;
        background-color: #071426;
        padding-left: 45%;
        padding-bottom:2%;
    }
    </style>
</head>
<body>
    <ul>
    <form action="{{url('store/processHeader')}}" method="post" >
        @csrf
        <li><a href="{{url('store/home')}}"><img src="{{asset('img/nav/logoetoys.png')}}" alt="" width="100" height="50"></a></li>
        <li><input class="active" type="submit" name="home" value="Store"></li>
        <li><input type="submit" name="lib" value="Library"></li>
        <li><input type="submit" name="prof" value="Profile"></li>
        <li><a style="position:absolute; float: left;" href="{{url('store/cart')}}"><img src="{{asset('img/cart/cart.png')}}" alt="" width="50" height="50"></a>
            <?php $cart=Session::get("cart")?>
            @if(Session::has("cart"))
            {{count($cart)}}
            @else
            0
            @endif
        </li>
    </form>
    </ul>
    <div id="isi">
    <h3 style="display:inline; position: absolute">Featured Games</h3>
    @if(Session::has("user"))
    <?php $user=Session::get("user");?>
    <div class="have">
        <a href="{{url('store/profile')}}"><img src="{{asset('img/profile/'.$user->foto)}}" alt="" width="55px" height="55px"></a>
        <form  action="{{url('store/processHeader')}}" method="post">
            @csrf
            Hello, <?= $user->username ?> <br> ETcoin: <?= $user->balance ?> <br>
            <input type="submit" name="out" value="Logout">
        </form>
        </div>
    @else
        <div class="have">
        Have an account?<br>
        <form action="{{url('store/processHeader')}}" method="post">
            @csrf
            <input type="submit" name="login" value="Login">
            <input type="submit" name="register" value="Sign Up">
        </form>
        </div>
    @endif
        <br><br><hr>
    <div class="slideshow-container">

    @foreach($listBanner as $row)
        <div class="mySlides fade">
            <div class="numbertext">1 / 3</div>
            <img src="{{asset('img/gameheader')."/".$row->foto}}" style="width:100%">
            <div class="text">{{$row->nama}}</div>
        </div>
    @endforeach

    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    </div>
    <br>

    <div style="text-align:center">
        <span class="dot" onclick="currentSlide(1)"></span>
        <span class="dot" onclick="currentSlide(2)"></span>
        <span class="dot" onclick="currentSlide(3)"></span>
    </div>
    <form action="{{url('store/processHome')}}" method="post" name="selectGame">
        @csrf
        @foreach($listGame as $row)
            <div class="fg" onclick="document.getElementById('namagame').value='{{$row->nama}}';document.forms['selectGame'].submit();">
                <div class="fgfoto">
                    <img src="{{asset('img/foto_game')."/".$row->foto}}" style="width:100%;height:100%">
                </div>
                <div class="judul">{{$row->nama}}</div><hr>
            </div>
        @endforeach
        <input type="hidden" name="nama" id="namagame" value="">
    </form>

    </div>
    <div id="footer">
    Copyright FAIPROJECT 2019.
    </div>
</body>
<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}
</script>
</html>
