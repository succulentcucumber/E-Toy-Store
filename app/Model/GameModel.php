<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GameModel extends Model
{
    protected $table = "game";
    protected $primaryKey = "nama";
    protected $connection = "mysql";
    public $timestamps = false;
    public $incrementing = false;
    protected $fillable = [
        'nama',
        'harga',
        'deskripsi',
        'foto'
    ];
}
