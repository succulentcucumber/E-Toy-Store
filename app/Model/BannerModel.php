<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BannerModel extends Model
{
    protected $table = "banner";
    protected $connection = "mysql";
    protected $primaryKey = "id";
    public $timestamps = false;
    protected $fillable = [
        'id',
        'nama',
        'foto'
    ];

}
